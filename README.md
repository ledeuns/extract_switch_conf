# extract_switch_conf

Extract configuration from CTCU switch

* extract_conf.sh: read configuration from switch (using *sh_conf.expect*) and cleanup output
* sh_conf.expect: Expect script to log on the switch and launch *show running-configuration*

### Usage

sh extract_conf.sh myswitch > myswitch_20200220.conf
